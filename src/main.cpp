//
// Graphics Framework.
// Copyright (C) 2007 Department of Computer Science, University of Copenhagen
//
#ifdef WIN32
#  define WIN32_LEAN_AND_MEAN
#  define NOMINMAX
#  include <windows.h>
#  undef WIN32_LEAN_AND_MEAN
#  undef NOMINMAX
#endif
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>


#include "graphics/graphics.h"
#include "solution/rasterizer.h"
#include "solution/math_types.h"
#include "solution/camera.h"
#include "solution/vertex_program.h"
#include "solution/fragment_program.h"


int winWidth = 1024;
int winHeight = 768;

using namespace graphics;

RenderPipeline<MyMathTypes>     render_pipeline;
MyVertexProgram<MyMathTypes>    vertex_program;
MyFragmentProgram<MyMathTypes>  fragment_program;
MyRasterizer<MyMathTypes>       rasterizer;
MyCamera<MyMathTypes>           camera;


void reshape( int width, int height )
{
  winWidth = width;
  winHeight = height;
  glViewport( 0, 0, winWidth, winHeight );
  glMatrixMode( GL_PROJECTION );
  glLoadIdentity();
  glMatrixMode( GL_MODELVIEW );
  glLoadIdentity();
  glutPostRedisplay();
};

void display()
{
  glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
  glMatrixMode( GL_MODELVIEW );
  //////////////////////////////////////////////////////////////////

  // >> TODO USE YOUR SOFTWARE RENDERPIPEINE TO DRAW << 

  MyMathTypes::vector3_type  v1( 500.0, 400.0, 0.0 );
  MyMathTypes::vector3_type  v2( 400.0, 500.0, 0.0 );
  MyMathTypes::vector3_type  v3( 400.0, 400.0, 0.0 );
  MyMathTypes::vector3_type  n1( 0.0, 0.0, 1.0 );
  MyMathTypes::vector3_type  n2( 0.0, 0.0, 1.0 );
  MyMathTypes::vector3_type  n3( 0.0, 0.0, 1.0 );
  MyMathTypes::vector3_type  c1( 1.0, 0.0, 0.0 );
  MyMathTypes::vector3_type  c2( 1.0, 0.0, 0.0 );
  MyMathTypes::vector3_type  c3( 1.0, 0.0, 0.0 );
  MyMathTypes::real_type     z = 1.0;
  MyMathTypes::vector3_type  color( 0.0, 1.0, 0.0 );

  //  camera.set_model_view( ...... );
  //  camera.set_projection( ...... );

  render_pipeline.clear( z, color );
  render_pipeline.draw_triangle( v1, n1, c1, v2, n2, c2, v3, n3, c3);
  render_pipeline.flush();
  
  //////////////////////////////////////////////////////////////////
  glFinish();
  glutSwapBuffers();
};

int main( int argc, char **argv )
{
  glutInit( &argc, argv );

  glutInitDisplayMode( GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE | GLUT_STENCIL);
  glutInitWindowSize( winWidth, winHeight );
  glutInitWindowPosition( 50, 50 );
  glutCreateWindow( "Graphics is Fun" );

  glClearColor( 1, 1, 1, 0 );
  //////////////////////////////////////////////////////////////////

  // >> TODO INITIALIZE YOUR RENDERPIPELINE << 

  //--- connect hardware
  render_pipeline.load_vertex_program( vertex_program );
  render_pipeline.load_fragment_program( fragment_program );
  render_pipeline.load_rasterizer( rasterizer);

  //--- allocate memory
  render_pipeline.set_resolution(1024, 768 );

  //--- set up graphics state
  render_pipeline.state().ambient_intensity() = 0.5;

  //--- init camera
  camera.init( render_pipeline );

  //////////////////////////////////////////////////////////////////
  glutDisplayFunc( display );
  glutReshapeFunc( reshape );
  glutMainLoop();
  return 0;
};
