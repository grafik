#ifndef GRAPHICS_RENDER_PIPELINE_H
#define GRAPHICS_RENDER_PIPELINE_H
//
// Graphics Framework.
// Copyright (C) 2007 Department of Computer Science, University of Copenhagen
//

#include "graphics_vertex_program.h"
#include "graphics_rasterizer.h"
#include "graphics_fragment_program.h"
#include "graphics_zbuffer.h"
#include "graphics_framebuffer.h"
#include "graphics_state.h"

namespace graphics
{

  /**
  * Render Pipeline.
  * This class implements a simple software driven hardware render pipeline.
  *
  * It mimics the real-life hardware without too many nifty-gritty details. It
  * consist basically of the following components:
  *
  *  - vertex program
  *  - rasterizer
  *  - fragment program
  *  - framebuffer
  *  - zbuffer
  *
  * The vertex program processes vertex data (coordinates, normals and colors)
  * specified by the end user (by invoking the draw_triangle method).
  *
  * The vertex program is responsible for performing the model view projection
  * transformation. Thus upon return from a vertex program the vertex data have
  * been transformed into screen-space.
  *
  * Prior to drawing a triangle end-users are responsible for setting up the
  * transformation matrices themself. This is done using the state-method to
  * retrieve the current state information about the render pipeline and then
  * invoke the model- and projection methods on the state instance to change
  * these matrices.
  *
  * The current graphics state is passed along to the vertex program, so it
  * can extract whatever information that it may need about the current state
  * of the render pipeline.
  *
  * Once vertex data have been transformed into screen-space the rasterizer are
  * asked to convert the triangle into fragment. A fragment is an entity that
  * potential ends up on the screen as a pixel. In order to do so the fragment
  * must survive all the way through the render pipeline and be written to the
  * frame buffer.
  *
  * The render pipeline contineously extract fragment information from the
  * rasterizer until no more fragments exist. For each fragment the render
  * pipeline performs a depth test using a z-buffer. If the depth-test is
  * passed then the fragment is being processed by a fragment program.
  *
  * The fragment program is responsible for computing the final color that
  * should be used when the fragment (now it will have become a pixel) is
  * written into the framebuffer. Like the vertex program the fragment program
  * is passed the current graphcis state, such that need information about
  * light and material can be extracted in order to compute the output color.
  *
  * To instantiate a render pipeline one needs to specify what math_types
  * that should be used. This is done by creating an empty class with all
  * the needed typedefs. (The compiler will output errors if one forgets a
  * typedef). A simple-minded example would be
  *
  *
  *  class MyMathTypes
  *  {
  *  public:
  *      typedeg float  real_type;
  *      typedeg float  vector3_type[3];
  *      typedeg float  vector4_type[4];
  *      typedeg float  matrix4x4_type[16];
  *  };
  *
  * Now one can create a render pipeline type by writting 
  *
  *   RenderPipeline<MyMathTypes>  render_pipeline;
  *
  * Once the render pipeline is created one needs to setup the ``hardware'' by
  * specifying what vertex-, fragement-, and rasterizers the render pipeline
  * should use.
  *
  * First one would need to create those components by inheriting from
  * the VertexProgram, FragmentProgram and Rasterizer base classes.
  *
  *   template<typename math_types>
  *   class MyVertexProgram : public VertexProgram<math_types>
  *   {
  *       // add your implementation here 
  *   };
  *
  *   template<typename math_types>
  *   class MyFragmentProgram : public FragmentProgram<math_types>
  *   {
  *       // add your implementation here 
  *   };
  *
  *   template<typename math_types>
  *   class MyRasterizer : public Rasterizer<math_types>
  *   {
  *       // add your implementation here 
  *   };
  *
  * Now create the objects and load them into the render pipeline.
  *
  *   MyVertexProgram<MyMathTypes>    vertex_program;
  *   MyFragmentProgram<MyMathTypes>  fragment_program;
  *   MyRasterizer<MyMathTypes>       rasterizer;
  *   render_pipeline.load_vertex_program( vertex_program );
  *   render_pipeline.load_fragment_program( fragment_program );
  *   render_pipeline.load_rasterizer( rasterizer);
  *
  * Now you have connected all the hardware the next task is to
  * allocate memory for the internal buffers of the render pipeline.
  * This is done by invoking the set_resolution method.
  *
  *  render_pipeline.set_resolution(1024, 768 );
  *
  * Before starting to render one needs to make sure that the graphics
  * state is setup correctly. In order to do so you will need to the
  * query the graphics state and modify whatever you need.
  *
  * render_pipeline.state().projection() = ....;
  * render_pipeline.state().model() = ....;
  * render_pipeline.state().light_position() = ....;
  * ...
  * render_pipeline.state().fall_off() = ....;
  *
  * Now we are ready to setup our renderloop. First we need to clear
  * the buffers, then draw the triangles and finally flush everything
  * onto the actual screen.
  *
  *  render_pipeline.clear( z_value, color );
  *  for( .... )
  *    render_pipeline.draw_triangle( .... );
  *  render_pipeline.flush();
  *
  * This is it, enjoy!
  */
  template< typename math_types >
  class RenderPipeline
  {
  public:

    typedef typename math_types::vector3_type     vector3_type;
    typedef typename math_types::vector4_type     vector4_type;
    typedef typename math_types::real_type        real_type;
    typedef typename math_types::matrix4x4_type   matrix4x4_type;

  public:

    typedef VertexProgram<math_types>            vertex_program_type;
    typedef Rasterizer<math_types>               rasterizer_type;
    typedef FragmentProgram<math_types>          fragment_program_type;
    typedef ZBuffer<math_types>                  zbuffer_type;
    typedef FrameBuffer<math_types>              frame_buffer_type;
    typedef GraphicsState<math_types>            graphics_state_type;

  protected:

    vertex_program_type    * m_vertex_program;
    fragment_program_type  * m_fragment_program;
    rasterizer_type        * m_rasterizer;

    zbuffer_type           m_zbuffer;
    frame_buffer_type      m_frame_buffer;
    graphics_state_type    m_state;

  public:

    RenderPipeline()
      : m_vertex_program(0)
      , m_fragment_program(0)
      , m_rasterizer(0)
    {
    }

  public:

    void load_vertex_program( vertex_program_type & program )      {   m_vertex_program = &program;      }
    void load_fragment_program( fragment_program_type & program )  {   m_fragment_program = &program;    }
    void load_rasterizer( rasterizer_type & rasterizer )           {   m_rasterizer = &rasterizer;       }

    /**
    * Set Resolution.
    *
    * @param width  The number of pixels in a row. Must be larger than 1 otherwise an exception is thrown.
    * @param height  The number of pixels in a colum. Must be larger than 1 otherwise an exception is thrown.
    */
    void set_resolution(int width, int height)
    {
      m_frame_buffer.set_resolution(width,height);
      m_zbuffer.set_resolution(width,height);
    }

    /**
    * Get Graphics State.
    *
    * @return The current graphics state.
    */
    graphics_state_type const & state() const {  return m_state; }
    graphics_state_type       & state()       {  return m_state; }


    /**
    * Clear Buffers.
    * This method should be used to setup the background color and z-values before
    * doing any kind of drawing.
    *
    * @param color   The color to be used to clear the buffer. Each color component must be in the interval [0..1] otherwise an exception is thrown.
    * @param depth   The value to be used to clear the buffer. Must be in the interval [0..1] otherwise an exception is thrown.
    *
    */
    void clear( real_type const & depth, vector3_type const & color)
    {
      m_frame_buffer.clear(color);
      m_zbuffer.clear(depth);
    }

    /**
    * Draw Triangle.
    * This method process the vertex data that makes up a triangle (3 coordinates,
    * 3  normals, and 3 colors). Vertex data is basically turned into pixels in
    * the framebuffer.
    *
    * Note: If renderpipeline is not correctly setup then an exception is thrown.
    *
    *
    * @param in_vertex1
    * @param in_normal1
    * @param in_color1
    * @param in_vertex2
    * @param in_normal2
    * @param in_color2
    * @param in_vertex3
    * @param in_normal3
    * @param in_color3
    */
    void draw_triangle(  
      vector3_type const & in_vertex1, vector3_type const & in_normal1, vector3_type const & in_color1
      , vector3_type const & in_vertex2, vector3_type const & in_normal2, vector3_type const & in_color2
      , vector3_type const & in_vertex3, vector3_type const & in_normal3, vector3_type const & in_color3
      )
    {
      //--- Test if render pipeline was set up correctly
      if(!m_vertex_program)
        throw std::logic_error("vertex program was not loaded");
      if(!m_rasterizer)
        throw std::logic_error("rasterizer was not loaded");
      if(!m_fragment_program)
        throw std::logic_error("fragment program was not loaded");

      //--- Temporaries used to hold output from vertex program
      vector3_type out_vertex1;
      vector3_type out_vertex2;
      vector3_type out_vertex3;
      vector3_type out_normal1;
      vector3_type out_normal2;
      vector3_type out_normal3;
      vector3_type out_color1;
      vector3_type out_color2;
      vector3_type out_color3;

      //--- Ask vertex program to process all the vertex data.
      m_vertex_program->run( 
        this->state(), in_vertex1, in_normal1, in_color1, out_vertex1, out_normal1,  out_color1 
        );
      m_vertex_program->run( 
        this->state(), in_vertex2, in_normal2, in_color2, out_vertex2, out_normal2,  out_color2 
        );
      m_vertex_program->run( 
        this->state(), in_vertex3, in_normal3, in_color3, out_vertex3, out_normal3,  out_color3 
        );

      //--- Initialize rasterizer with output from the vertex program
      m_rasterizer->init( 
        out_vertex1, out_normal1, out_color1
        , out_vertex2, out_normal2, out_color2
        , out_vertex3, out_normal3, out_color3
        );

      //--- Keep on processing fragments until there are none left
      while( m_rasterizer->more_fragments() )
      {
        //--- get screen location of the current fragment
        int screen_x = m_rasterizer->x();
        int screen_y = m_rasterizer->y();

        //--- extract old and new z value and perform a z-test
        real_type z_old = m_zbuffer.read( screen_x, screen_y );
        real_type z_new = m_rasterizer->depth();

        if(  this->state().ztest( z_old, z_new ) )
        {
          //--- The fragment passed the z-test, now we need to ask
          //--- the fragment program to compute the color of the fragment.
          vector3_type out_color;

          m_fragment_program->run(  
            this->state()
            , m_rasterizer->position()
            , m_rasterizer->normal()
            , m_rasterizer->color()
            , out_color 
            );

          //--- Finally we write the new z-value to the z-buffer
          //--- and the new color to the frame buffer.
          m_zbuffer.write( screen_x, screen_y, z_new);
          m_frame_buffer.write(screen_x, screen_y, out_color);
        }

        //--- We finished processing the fragment, so we
        //--- can advance to the next fragment.
        m_rasterizer->next_fragment();
      }
    }

    /**
    * Flush to Screen.
    * When this method is invoked whatever content of
    * the framebuffer will be shown on the screen.
    *
    * This method should be invoked when finished
    * drawing all triangles.
    */
    void flush()
    {
      m_frame_buffer.flush();
    }

  };

}// end namespace graphics

// GRAPHICS_RENDER_PIPELINE_H
#endif
