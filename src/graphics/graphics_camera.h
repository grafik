#ifndef GRAPHICS_CAMERA_H
#define GRAPHICS_CAMERA_H
//
// Graphics Framework.
// Copyright (C) 2008 Department of Computer Science, University of Copenhagen
//

namespace graphics
{
  /**
   * A virtual pin-hole camera.
   * The camera class produces the matrices necessary for the virtual world
   * to be rasterized.
   */
  template<typename math_types>
  class Camera
  {
  public:

    typedef typename math_types::matrix4x4_type      matrix4x4_type;
    typedef typename math_types::vector3_type      vector3_type;
	typedef typename math_types::vector2_type      vector2_type;
    typedef typename math_types::real_type         real_type;

  protected:

    GraphicsState<math_types> * m_state;

  public:

    Camera()
      : m_state(0)
      {}

    /**
     *
     */
    void init( RenderPipeline<math_types> & R ) { 
		m_state = &(R.state());
	};

    /**
    * Set projection matrix.
    *
    * @param vrp	View reference point
    * @param vpn View-plane normal 
    * @param vup	View up vector
    * @param prp	Projection reference point
    * @param lower_left	Lower left corner of the view-plane
    * @param upper_right	Upper right corner of the view-plane
    * @param front_plane		Distance to front clipping plane
    * @param back_plane	Distance to back clipping plane
    */
    void set_projection( 
             vector3_type const & vrp
           , vector3_type const & vpn
           , vector3_type const & vup
           , vector3_type const & prp
           , vector2_type const & lower_left
           , vector2_type const & upper_right
           , real_type const & front_plane
           , real_type const & back_plane 
          )
    {
      m_state->projection() = this->compute_projection_matrix( vrp, vpn, vup, prp, lower_left,  upper_right, front_plane, back_plane );
    };

	/**
    * Set model-view matrix.
	*
	*@param M	Model-View matrix.
    */
	void set_model_view(matrix4x4_type const & M)
    {
		m_state->model() = M;
    };


  protected:

    /**
	* Computes a projection matrix using the parameters for a perspective camera 
	* given by Foley et al. (p.229-284; 2nd edition)
    * The projection matrix should transforms from world coordinates to 
	* normalized projection coordinates
	*
    * @param vrp	View reference point
    * @param vpn View-plane normal 
    * @param vup	View up vector
    * @param prp	Projection reference point
    * @param lower_left	Lower left corner of the view-plane
    * @param upper_right	Upper right corner of the view-plane
    * @param front_plane		Distance to front clipping plane
    * @param back_plane	Distance to back clipping plane
    */
    virtual matrix4x4_type compute_projection_matrix(
             vector3_type const & vrp
           , vector3_type const &  vpn
           , vector3_type const &  vup
           , vector3_type const & prp
           , vector2_type const &  lower_left
           , vector2_type const &  upper_right
           , real_type const & front_plane
           , real_type const & back_plane 
	  ) = 0;

  };

}// end namespace graphics

// GRAPHICS_CAMERA_H
#endif
