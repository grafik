#ifndef GRAPHICS_STATE_H
#define GRAPHICS_STATE_H
//
// Graphics Framework.
// Copyright (C) 2007 Department of Computer Science, University of Copenhagen
//

namespace graphics
{

  template< typename math_types >
  class GraphicsState
  {
  public:

    typedef typename math_types::vector3_type     vector3_type;
    typedef typename math_types::vector4_type     vector4_type;
    typedef typename math_types::real_type        real_type;
    typedef typename math_types::matrix4x4_type   matrix4x4_type;

  protected:

    //--- Model to camera space transformations
    matrix4x4_type  m_projection;
    matrix4x4_type  m_model;

    // Light source setting
    vector3_type m_light_position;

    // Material setting
    vector3_type    m_ambient_color;
    vector3_type    m_diffuse_color;
    vector3_type    m_specular_color;

    real_type       m_fall_off;
    real_type       m_ambient_intensity;
    real_type       m_diffuse_intensity;
    real_type       m_specular_intensity;

  public:

    matrix4x4_type const & projection() const { return m_projection; }
    matrix4x4_type       & projection()       { return m_projection; }

    matrix4x4_type const & model() const { return m_model; }
    matrix4x4_type       & model()       { return m_model; }

    vector3_type const & light_position() const { return m_light_position; }
    vector3_type       & light_position()       { return m_light_position; }

    vector3_type const & ambient_color()  const { return m_ambient_color;  }
    vector3_type       & ambient_color()        { return m_ambient_color;  }
    
    vector3_type const & diffuse_color()  const { return m_diffuse_color;  }
    vector3_type       & diffuse_color()        { return m_diffuse_color;  }
    
    vector3_type const & specular_color() const { return m_specular_color; }
    vector3_type       & specular_color()       { return m_specular_color; }

    real_type const & ambient_intensity()  const { return m_ambient_intensity;  }
    real_type       & ambient_intensity()        { return m_ambient_intensity;  }
    
    real_type const & diffuse_intensity()  const { return m_diffuse_intensity;  }
    real_type       & diffuse_intensity()        { return m_diffuse_intensity;  }
    
    real_type const & specular_intensity() const { return m_specular_intensity; }
    real_type       & specular_intensity()       { return m_specular_intensity; }


    real_type const & fall_off () const { m_fall_off; }
    real_type       & fall_off ()       { m_fall_off; }

    bool ztest(real_type const & z_old, real_type const & z_new) { return (z_new<z_old); }

  };

}// end namespace graphics

// GRAPHICS_STATE_H
#endif
