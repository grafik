#ifndef CAMERA_H
#define CAMERA_H
//
// Graphics Framework.
// Copyright (C) 2007 Department of Computer Science, University of Copenhagen
//
#include "graphics/graphics.h"

namespace graphics {

  template<typename math_types>
    class MyCamera : public Camera<math_types>
  {
  public:

    typedef typename math_types::matrix4x4_type    matrix4x4_type;
    typedef typename math_types::vector3_type      vector3_type;
    typedef typename math_types::vector2_type      vector2_type;
    typedef typename math_types::real_type         real_type;

  protected:

    /**
     *
     */
    matrix4x4_type compute_projection_matrix(
             vector3_type const & vrp
           , vector3_type const & vpn
           , vector3_type const & vup
           , vector3_type const & prp
           , vector2_type const & lower_left
           , vector2_type const & upper_right
           , real_type const & front_plane
           , real_type const & back_plane 
	  )
    {		
      // ToDo add your magic here!
		matrix4x4_type M;
		M[1][1] = 1, M[1][2] = 0, M[1][3] = 0, M[1][4] = 1;
		M[2][1] = 0, M[2][2] = 1, M[2][3] = 0, M[2][4] = 1; 
		M[3][1] = 0, M[3][2] = 0, M[3][3] = 1, M[3][4] = 1; 
		M[4][1] = 0, M[4][2] = 0, M[4][3] = 0, M[4][4] = 1; 

		//return the identity matrix
		return M;
    };
  };
}// end namespace graphics

// CAMERA_H
#endif
