#ifndef RASTERIZER_H
#define RASTERIZER_H
//
// Graphics Framework.
// Copyright (C) 2007 Department of Computer Science, University of Copenhagen
//

#include "graphics/graphics.h"

namespace graphics {

  template<typename math_types>
  class MyRasterizer : public Rasterizer<math_types>
  {
  public:

    typedef typename math_types::vector3_type      vector3_type;
    typedef typename math_types::real_type         real_type;



  protected:


  public:

    MyRasterizer()
    {
    }


    virtual ~MyRasterizer()
    {
    }


    void init(  
      vector3_type const & in_vertex1, vector3_type const & in_normal1, vector3_type const & in_color1
      , vector3_type const & in_vertex2, vector3_type const & in_normal2, vector3_type const & in_color2
      , vector3_type const & in_vertex3, vector3_type const & in_normal3, vector3_type const & in_color3
      ) 
    {
      // TODO Add your own magic here....
    }

    int x() const      
    {  
      // TODO Add your own magic here....

      return 0;     
    }

    int y() const
    {
      // TODO Add your own magic here....

      return 0;     
    }

    real_type depth() const     
    {
      // TODO Add your own magic here....

      return 0.0;     
    }

    bool more_fragments() const 
    {
      // TODO Add your own magic here....

      return false; 
    }

    vector3_type position() const 
    {
      // TODO Add your own magic here....

      return vector3_type();
    }

    vector3_type normal() const     
    {
      // TODO Add your own magic here....

      return vector3_type();    
    }

    vector3_type color() const 
    {
      // TODO Add your own magic here....

      return vector3_type();
    }

    void next_fragment()    
    {
      // TODO Add your own magic here....
    }
  };

}// end namespace graphics

// RASTERIZER_H
#endif
